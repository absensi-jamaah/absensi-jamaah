<div class="col-md-6 mt-5">
    <div class="d-flex justify-content-between mb-n3">
        <div>
            <h5>NPWP (OPSIONAL)</h5>
        </div>
        <div>
            <h6><a href="<?php echo base_url('akun/ubah_npwp')?>">Ubah</a></h6>
        </div>
    </div>
    <hr>
    <div class="d-flex justify-content-between mb-n4 mt-4 px-2">
        <div>
            <p class="text-secondary">Nama NPWP</p>
        </div>
        <div>
            <p class="fw-bold"><?php echo $akun['nama_npwp'] ?></p>
        </div>
    </div>
    <hr>
    <div class="d-flex justify-content-between mb-n4 mt-4 px-2">
        <div>
            <p class="text-secondary">Nomor NPWP</p>
        </div>
        <div>
            <p class="fw-bold"><?php echo $akun['nomor_npwp'] ?></p>
        </div>
    </div>
    <hr>
</div>