<div class="col-md-6">
    <div class="d-flex justify-content-between mb-n3">
        <div>
            <h5>AKUN KASIR</h5>
        </div>
        <div>
            <h6><a href="<?php echo base_url('akun/akun_kasir')?>">Detail</a></h6>
        </div>
    </div>
    <hr>
    <div class="d-flex justify-content-between mb-n4 mt-4 px-2">
        <div>
            <p class="text-secondary">Nama Kasir</p>
        </div>
        <div>
            <p class="fw-bold">Nama Kasir 1</p>
        </div>
    </div>
    <hr>
    <div class="d-flex justify-content-between mb-n4 mt-4 px-2">
        <div>
            <p class="text-secondary">Nama Kasir</p>
        </div>
        <div>
            <p class="fw-bold">Nama Kasir 2</p>
        </div>
    </div>
    <hr>
    <div class="d-flex justify-content-between mb-n4 mt-4 px-2">
        <div>
            <p class="text-secondary">Nama Kasir</p>
        </div>
        <div>
            <p class="fw-bold">Nama Kasir 3</p>
        </div>
    </div>
    <hr>
</div>