<table class="table">
    <thead class="table-light">
        <tr>
            <th>Tipe Penjualan</th>
            <th class="text-end">Jumlah Transaksi</th>
            <th class="text-end">Total Dikumpulkan</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="text-start p-2 fw-medium border-end">Grab Food</td>
            <td class="text-end p-2">1</td>
            <td class="text-end p-2">Rp. 0</td>
        </tr>
        <tr>
            <td class="text-start p-2 fw-medium border-end">Toko</td>
            <td class="text-end p-2">52</td>
            <td class="text-end p-2">Rp. 4.979.000</td>
        </tr>
        <tr>
            <td class="text-start p-2 fw-bold border-end">Total</td>
            <td class="text-end p-2 fw-bold"></td>
            <td class="text-end p-2 fw-bold">Rp. 4.979.000</td>
        </tr>
    </tbody>
</table>