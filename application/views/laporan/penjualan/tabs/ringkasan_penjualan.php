<table class="table">
    <thead class="table-light">
        <tr>
            <th colspan="2"></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="text-start p-2">Penjualan kotor</td>
            <td class="text-end p-2">Rp. 1.170.000</td>
        </tr>
        <tr>
            <td class="text-start p-2">Diskon</td>
            <td class="text-end p-2">(Rp. 5.200)</td>
        </tr>
        <tr>
            <td class="text-start p-2">Pengembalian dana</td>
            <td class="text-end p-2">Rp. 0</td>
        </tr>
        <tr>
            <td class="text-start p-2 fw-bold">Penjualan bersih</td>
            <td class="text-end p-2 fw-bold">Rp. 1.164.800</td>
        </tr>
        <tr>
            <td class="text-start p-2">Persen</td>
            <td class="text-end p-2">Rp. 0</td>
        </tr>
        <tr>
            <td class="text-start p-2">Pajak</td>
            <td class="text-end p-2">Rp. 0</td>
        </tr>
        <tr>
            <td class="text-start p-2">Pembulatan</td>
            <td class="text-end p-2">Rp. 0</td>
        </tr>
        <tr>
            <td class="text-start p-2 fw-bold">Total Dikumpulkan</td>
            <td class="text-end p-2 fw-bold">Rp. 1.164.800</td>
        </tr>
    </tbody>
</table>