<!-- Nav tabs -->
<ul class="nav nav-tabs">
    <li class="nav-item">
        <a class="nav-link active" data-bs-toggle="tab" href="#penghasilan">Penghasilan</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" data-bs-toggle="tab" href="#kuantitas">Kuantitas</a>
    </li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
    <div class="tab-pane container active" id="penghasilan">
        <div class="table-penjualan">
            <table class="table table-penjualan">
                <thead class="table-light">
                    <tr>
                        <th>Nama</th>
                        <th class="text-end text-truncate">SKU</th>
                        <th class="text-end text-truncate">Kategori</th>
                        <th class="text-end text-truncate">Barang Terjual</th>
                        <th class="text-end text-truncate">Barang Dikembalikan</th>
                        <th class="text-end text-truncate">Penjualan Kotor</th>
                        <th class="text-end text-truncate">Diskon</th>
                        <th class="text-end text-truncate">Pengembalian Dana</th>
                        <th class="text-end text-truncate">Penjualan Bersih</th>
                        <th class="text-end text-truncate">COGS</th>
                        <th class="text-end text-truncate">Laba Kotor</th>
                        <th class="text-end text-truncate">Margin Kotor</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-start p-2 text-truncate fw-medium border-end">Bandeng Presto Juwana Erlina isi 2
                            0.35K</td>
                        <td class="text-end p-2 text-truncate"></td>
                        <td class="text-end p-2 text-truncate">Produk Samsae</td>
                        <td class="text-end p-2 text-truncate">2</td>
                        <td class="text-end p-2 text-truncate"></td>
                        <td class="text-end p-2 text-truncate">Rp. 148.000</td>
                        <td class="text-end p-2 text-truncate">Rp. 0</td>
                        <td class="text-end p-2 text-truncate">Rp. 0</td>
                        <td class="text-end p-2 text-truncate">Rp. 148.000</td>
                        <td class="text-end p-2 text-truncate">(Rp. 119.000)</td>
                        <td class="text-end p-2 text-truncate">Rp. 29.000</td>
                        <td class="text-end p-2 text-truncate">20%</td>
                    </tr>
                    <tr>
                        <td class="text-start p-2 fw-bold border-end">Total</td>
                        <td class="text-end p-2 text-truncate fw-bold"></td>
                        <td class="text-end p-2 text-truncate fw-bold"></td>
                        <td class="text-end p-2 text-truncate fw-bold">2</td>
                        <td class="text-end p-2 text-truncate fw-bold">0</td>
                        <td class="text-end p-2 text-truncate fw-bold">Rp. 148.000</td>
                        <td class="text-end p-2 text-truncate fw-bold">Rp. 0</td>
                        <td class="text-end p-2 text-truncate fw-bold">Rp. 0</td>
                        <td class="text-end p-2 text-truncate fw-bold">Rp. 148.000</td>
                        <td class="text-end p-2 text-truncate fw-bold">(Rp. 119.000)</td>
                        <td class="text-end p-2 text-truncate fw-bold">Rp. 29.000</td>
                        <td class="text-end p-2 text-truncate fw-bold">20%</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="tab-pane container fade" id="kuantitas">
        <div class="table-penjualan">
            <table class="table table-penjualan">
                <thead class="table-light">
                    <tr>
                        <th>Nama</th>
                        <th class="text-end text-truncate">SKU</th>
                        <th class="text-end text-truncate">Kategori</th>
                        <th class="text-end text-truncate">Kuantitas Terjual</th>
                        <th class="text-end text-truncate">Pengembalian Kuantitas</th>
                        <th class="text-end text-truncate">Total Terjual</th>
                        <th class="text-end text-truncate">Jumlah Dikembalikan</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-start p-2 text-truncate fw-medium border-end">Bandeng Presto Juwana Erlina isi 2
                            0.35K</td>
                        <td class="text-end p-2 text-truncate"></td>
                        <td class="text-end p-2 text-truncate">Produk Samsae</td>
                        <td class="text-end p-2 text-truncate">2</td>
                        <td class="text-end p-2 text-truncate">0</td>
                        <td class="text-end p-2 text-truncate">2</td>
                        <td class="text-end p-2 text-truncate">0</td>
                    </tr>
                    <tr>
                        <td class="text-start p-2 fw-bold border-end">Total</td>
                        <td class="text-end p-2 text-truncate fw-bold"></td>
                        <td class="text-end p-2 text-truncate fw-bold"></td>
                        <td class="text-end p-2 text-truncate fw-bold">2</td>
                        <td class="text-end p-2 text-truncate fw-bold">0</td>
                        <td class="text-end p-2 text-truncate fw-bold">2</td>
                        <td class="text-end p-2 text-truncate fw-bold">0</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>