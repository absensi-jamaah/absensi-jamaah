<div class="table-penjualan">
    <table class="table table-penjualan">
        <thead class="table-light">
            <tr>
                <th>Nama</th>
                <th class="text-end text-truncate">Jumlah Diskon</th>
                <th class="text-end text-truncate">Jumlah</th>
                <th class="text-end text-truncate">Diskon Koter</th>
                <th class="text-end text-truncate">Diskon Dikembalikan</th>
                <th class="text-end text-truncate">Diskon Bersih</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="text-start p-2 text-truncate fw-medium border-end">SO Bakpia</td>
                <td class="text-end p-2 text-truncate">5%</td>
                <td class="text-end p-2 text-truncate">7</td>
                <td class="text-end p-2 text-truncate">(Rp. 24.000) </td>
                <td class="text-end p-2 text-truncate"></td>
                <td class="text-end p-2 text-truncate">(Rp. 24.000)0</td>
            </tr>
            <tr>
                <td class="text-start p-2 text-truncate fw-medium border-end">SO Samsae</td>
                <td class="text-end p-2 text-truncate">10%</td>
                <td class="text-end p-2 text-truncate">1</td>
                <td class="text-end p-2 text-truncate">(Rp. 4.800)</td>
                <td class="text-end p-2 text-truncate"></td>
                <td class="text-end p-2 text-truncate">(Rp. 4.800)</td>
            </tr>
            <tr>
                <td class="text-start p-2 fw-bold border-end">Total</td>
                <td class="text-end p-2 text-truncate fw-bold"></td>
                <td class="text-end p-2 text-truncate fw-bold">8</td>
                <td class="text-end p-2 text-truncate fw-bold">(Rp. 28.800)</td>
                <td class="text-end p-2 text-truncate fw-bold"></td>
                <td class="text-end p-2 text-truncate fw-bold">(Rp. 28.800)</td>
            </tr>
        </tbody>
    </table>
</div>