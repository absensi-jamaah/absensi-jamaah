<div>
    <table class="table table-penjualan">
        <thead class="table-light">
            <tr>
                <th>Nama</th>
                <th class="text-end text-truncate">Role</th>
                <th class="text-end text-truncate">Jumlah Transaksi</th>
                <th class="text-end text-truncate">Total DIdapatkan</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="text-start p-2 text-truncate fw-medium">Dina Kasir 2</td>
                <td class="text-end p-2 text-truncate">Employee</td>
                <td class="text-end p-2 text-truncate">10</td>
                <td class="text-end p-2 text-truncate">Rp. 911.000</td>
            </tr>
            <tr>
                <td class="text-start p-2 text-truncate fw-medium">Kasir Samsae Ngaliyan</td>
                <td class="text-end p-2 text-truncate">Employee</td>
                <td class="text-end p-2 text-truncate">6</td>
                <td class="text-end p-2 text-truncate">Rp. 450.700</td>
            </tr>
            <td class="text-start p-2 text-truncate fw-medium">Kasir Anjasmoro</td>
            <td class="text-end p-2 text-truncate">Employee</td>
            <td class="text-end p-2 text-truncate">2</td>
            <td class="text-end p-2 text-truncate">Rp. 490.000</td>
            </tr>
            <tr>
                <td class="text-start p-2 fw-bold">Total</td>
                <td class="text-end p-2 text-truncate fw-bold"></td>
                <td class="text-end p-2 text-truncate fw-bold">18</td>
                <td class="text-end p-2 text-truncate fw-bold">Rp. 1.851.700</td>
            </tr>
        </tbody>
    </table>
</div>