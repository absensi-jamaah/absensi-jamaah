<div class="card">
    <div class="card-header p-1 rounded-top bg-primary">

    </div>
    <div class="card-body border rounded-bottom">
        <h5 class="card-title">Laba Kotor</h5>
        <p class="card-text">Laba Kotor adalah Penjualan Bersih Anda dikurangi Harga Pokok Penjualan (COGS). Untuk
            melaporkan laba kotor secara akurat, pastikan semua item memiliki HPP.
        </p>
    </div>
</div>
<table class="table">
    <thead class="table-light">
        <tr>
            <th colspan="3"></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="text-start p-2">Penjualan Kotor</td>
            <td class="text-end p-2">Rp. 1.959.590.800</td>
        </tr>
        <tr>
            <td class="text-start p-2">Diskon</td>
            <td class="text-end p-2">(Rp. 7.307.750)</td>
        </tr>
        <tr class="border-dark">
            <td class="text-start p-2">Dikembalikan</td>
            <td class="text-end p-2">(Rp. 20.209.250)</td>
            <td class="text-end p-2"></td>
        </tr>
        <tr>
            <td class="text-start p-2 fw-bold">Penjualan Bersih</td>
            <td class="text-end p-2 fw-bold">Rp. 1.932.073.800</td>
            <td class="text-start p-2 fw-bold">
                <p class="bg-success text-white w-50 text-center rounded-pill px-1 py-1">100%</p>
            </td>
        </tr>
        <tr>
            <td class="text-start p-2">Harga Pokok Penjualan</td>
            <td class="text-end p-2">(Rp. 1.550.838.073)</td>
            <td class="text-start p-2 fw-bold">
                <p class="bg-warning text-white w-50 text-center rounded-pill px-1 py-1">100%</p>
            </td>
        </tr>
        <tr>
            <td class="text-start p-2 fw-bold">Laba Kotor</td>
            <td class="text-end p-2 fw-bold">Rp. 381.235.727</td>
            <td class="text-start p-2 fw-bold">
                <p class="bg-success text-white w-50 text-center rounded-pill px-1 py-1">100%</p>
            </td>
        </tr>
    </tbody>
</table>