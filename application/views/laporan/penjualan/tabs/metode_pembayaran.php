<table class="table">
    <thead class="table-light">
        <tr>
            <th>Metode Pembayaran</th>
            <th class="text-end">Jumlah Transaksi</th>
            <th class="text-end">Total Dikumpulkan</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="text-start p-2 fw-medium border-end">Uang Tunai</td>
            <td class="text-end p-2">76 </td>
            <td class="text-end p-2">Rp. 8.058.000</td>
        </tr>
        <tr>
            <td class="text-start p-2 fw-medium border-end">EDC</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td class="text-start py-2 px-3 text-secondary border-end">BCA</td>
            <td class="text-end p-2">6</td>
            <td class="text-end p-2">Rp. 725.000</td>
        </tr>
        <tr>
            <td class="text-start py-2 px-3 text-secondary border-end">EDC lainnya</td>
            <td class="text-end p-2">6</td>
            <td class="text-end p-2">Rp. 420.000</td>
        </tr>
        <tr>
            <td class="text-start p-2 fw-medium border-end">Lainnya</td>
            <td></td>
            <td></td>
        </tr>
        <tr>
            <td class="text-start py-2 px-3 text-secondary border-end">QR BCA</td>
            <td class="text-end p-2">8</td>
            <td class="text-end p-2">Rp. 897.200</td>
        </tr>
        <tr>
            <td class="text-start py-2 px-3 text-secondary border-end">Transfer Bank</td>
            <td class="text-end p-2">1</td>
            <td class="text-end p-2">Rp. 768.000</td>
        </tr>
        <tr>
            <td class="text-start p-2 fw-bold border-end">Total</td>
            <td class="text-end p-2 fw-bold">97</td>
            <td class="text-end p-2 fw-bold">Rp. 10.868.200</td>
        </tr>
    </tbody>
</table>