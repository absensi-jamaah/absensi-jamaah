<div class="table-penjualan">
    <table class="table table-penjualan">
        <thead class="table-light">
            <tr>
                <th>Kategori</th>
                <th class="text-end text-truncate">Barang Terjual</th>
                <th class="text-end text-truncate">Barang Dikembalikan</th>
                <th class="text-end text-truncate">Penjualan Kotor</th>
                <th class="text-end text-truncate">Diskon</th>
                <th class="text-end text-truncate">Refund</th>
                <th class="text-end text-truncate">Penjualan Bersih</th>
                <th class="text-end text-truncate">COGS</th>
                <th class="text-end text-truncate">Laba Kotor</th>
                <th class="text-end text-truncate">Margin Kotor</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="text-start p-2 text-truncate fw-medium border-end">BKJ Reguler</td>
                <td class="text-end p-2 text-truncate">3</td>
                <td class="text-end p-2 text-truncate"></td>
                <td class="text-end p-2 text-truncate">Rp. 109.000</td>
                <td class="text-end p-2 text-truncate">Rp. 0</td>
                <td class="text-end p-2 text-truncate">Rp. 0</td>
                <td class="text-end p-2 text-truncate">Rp. 109.000</td>
                <td class="text-end p-2 text-truncate">(Rp. 91.000)</td>
                <td class="text-end p-2 text-truncate">Rp. 18.000</td>
                <td class="text-end p-2 text-truncate">17%</td>
            </tr>
            <tr>
                <td class="text-start p-2 text-truncate fw-medium border-end">BKTJ Travel Pack</td>
                <td class="text-end p-2 text-truncate">24</td>
                <td class="text-end p-2 text-truncate"></td>
                <td class="text-end p-2 text-truncate">Rp. 1.152.000</td>
                <td class="text-end p-2 text-truncate">(Rp. 26.400)</td>
                <td class="text-end p-2 text-truncate">Rp. 0</td>
                <td class="text-end p-2 text-truncate">Rp. 1.125.600</td>
                <td class="text-end p-2 text-truncate">(Rp. 1.008.000)</td>
                <td class="text-end p-2 text-truncate">Rp. 117.600</td>
                <td class="text-end p-2 text-truncate">10%</td>
            </tr>
            <tr>
                <td class="text-start p-2 fw-bold border-end">Total</td>
                <td class="text-end p-2 text-truncate fw-bold">27</td>
                <td class="text-end p-2 text-truncate fw-bold"></td>
                <td class="text-end p-2 text-truncate fw-bold">Rp. 2.152.000</td>
                <td class="text-end p-2 text-truncate fw-bold">(Rp. 26.400)</td>
                <td class="text-end p-2 text-truncate fw-bold">Rp. 0</td>
                <td class="text-end p-2 text-truncate fw-bold">Rp. 2.125.600</td>
                <td class="text-end p-2 text-truncate fw-bold">(Rp. 2.008.000)</td>
                <td class="text-end p-2 text-truncate fw-bold">Rp. 241.600</td>
                <td class="text-end p-2 text-truncate fw-bold">12%</td>
            </tr>
        </tbody>
    </table>
</div>