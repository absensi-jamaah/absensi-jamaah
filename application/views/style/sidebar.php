<aside class="left-sidebar" style="background-color: #ffff ">
    <div class="scroll-sidebar" style="background-color: #ffff ">
        <nav class="sidebar-nav" style="background-color: #ffff ">
            <ul id="sidebarnav" style="background-color: #ffff ">
                <li class="sidebar-item mt-3">
                    <a href="<?php echo base_url('dashboard')?>"
                        class="sidebar-link waves-effect waves-dark sidebar-link" aria-expanded="false"><i
                            data-feather="home" class="feather-icon"></i><span class="hide-menu">Dashboard</span></a>
                </li>
                <li class="sidebar-item mt-3">
                    <a href="<?php echo base_url('data_jamaah')?>"
                        class="sidebar-link waves-effect waves-dark sidebar-link" aria-expanded="false"><i
                            data-feather="home" class="feather-icon"></i><span class="hide-menu">Data Jama'ah</span></a>
                </li>
                <li class="sidebar-item mt-3">
                    <a href="<?php echo base_url('daftar_kegiatan')?>"
                        class="sidebar-link waves-effect waves-dark sidebar-link" aria-expanded="false"><i
                            data-feather="home" class="feather-icon"></i><span class="hide-menu">Daftar Kegiatan</span></a>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i
                            data-feather="clipboard" class="feather-icon" style="background-color: #ffff "></i><span
                            class="hide-menu">Laporan
                        </span></a>
                    <ul aria-expanded="false" class="collapse first-level" style="background-color: #ffff ">
                        <li class="sidebar-item">
                            <a href="<?php echo base_url('penjualan')?>" class="sidebar-link"><i
                                    data-feather="disc"></i><span class="hide-menu">
                                    Penjualan
                                </span></a>
                        </li>
                        <li class="sidebar-item">
                            <a href="<?php echo base_url('transaksi')?>" class="sidebar-link"><i
                                    data-feather="disc"></i><span class="hide-menu">
                                    Transaksi
                                </span></a>
                        </li>
                        <li class="sidebar-item">
                            <a href="<?php echo base_url('bukti_pembayaran')?>" class="sidebar-link"><i
                                    data-feather="disc"></i><span class="hide-menu"> Bukti
                                    Pembayaran
                                </span></a>
                        </li>
                        <li class="sidebar-item">
                            <a href="<?php echo base_url('shift')?>" class="sidebar-link"><i
                                    data-feather="disc"></i><span class="hide-menu"> Shift
                                </span></a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i
                            data-feather="shopping-bag" class="feather-icon"></i><span class="hide-menu">Pustaka
                        </span></a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="sidebar-item">
                            <a href="<?php echo base_url('produk')?>" class="sidebar-link"><i data-feather="disc"
                                    class="mdi mdi-view-quilt"></i><span class="hide-menu"> Produk
                                </span></a>
                        </li>
                        <li class="sidebar-item">
                            <a href="<?php echo base_url('kategori')?>" class="sidebar-link"><i data-feather="disc"
                                    class="mdi mdi-view-quilt"></i><span class="hide-menu"> Kategori
                                </span></a>
                        </li>
                        <li class="sidebar-item">
                            <a href="<?php echo base_url('diskon')?>" class="sidebar-link"><i data-feather="disc"
                                    class="mdi mdi-view-quilt"></i><span class="hide-menu"> Diskon
                                </span></a>
                        </li>
                        <li class="sidebar-item">
                            <a href="<?php echo base_url('tipe_penjualan')?>" class="sidebar-link"><i
                                    data-feather="disc" class="mdi mdi-view-quilt"></i><span class="hide-menu"> Tipe
                                    Penjualan
                                </span></a>
                        </li>
                        <li class="sidebar-item">
                            <a href="<?php echo base_url('merek')?>" class="sidebar-link"><i data-feather="disc"
                                    class="mdi mdi-view-quilt"></i><span class="hide-menu"> Jenis
                                    Merek
                                </span></a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i
                            data-feather="package" class="feather-icon"></i><span class="hide-menu">Gudang
                        </span></a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="sidebar-item">
                            <a href="<?php echo base_url('ringkasan')?>" class="sidebar-link"><i data-feather="disc"
                                    class="mdi mdi-view-quilt"></i><span class="hide-menu">
                                    Ringkasan
                                </span></a>
                        </li>
                        <li class="sidebar-item">
                            <a href="<?php echo base_url('pesanan')?>" class="sidebar-link"><i data-feather="disc"
                                    class="mdi mdi-view-quilt"></i><span class="hide-menu"> Pesanan
                                    Pembelian
                                </span></a>
                        </li>
                        <li class="sidebar-item">
                            <a href="<?php echo base_url('transfer')?>" class="sidebar-link"><i data-feather="disc"
                                    class="mdi mdi-view-quilt"></i><span class="hide-menu"> Transfer
                                </span></a>
                        </li>
                        <li class="sidebar-item">
                            <a href="<?php echo base_url('penyesuaian')?>" class="sidebar-link"><i data-feather="disc"
                                    class="mdi mdi-view-quilt"></i><span class="hide-menu">
                                    Penyesuaian
                                </span></a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i
                            data-feather="users" class="feather-icon"></i><span class="hide-menu">Karyawan
                        </span></a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="sidebar-item">
                            <a href="<?php echo base_url('slot_karyawan')?>" class="sidebar-link"><i data-feather="disc"
                                    class="mdi mdi-view-quilt"></i><span class="hide-menu"> Slot
                                    Karyawan
                                </span></a>
                        </li>
                        <li class="sidebar-item">
                            <a href="<?php echo base_url('akses_karyawan')?>" class="sidebar-link"><i
                                    data-feather="disc" class="mdi mdi-view-quilt"></i><span class="hide-menu"> Akses
                                    Karyawan
                                </span></a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i
                            data-feather="credit-card" class="feather-icon"></i><span class="hide-menu">Pembayaran
                        </span></a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="sidebar-item">
                            <a href="<?php echo base_url('metode_pembayaran')?>" class="sidebar-link"><i
                                    data-feather="disc" class="mdi mdi-view-quilt"></i><span class="hide-menu"> Metode
                                    Pembayaran
                                </span></a>
                        </li>
                        <li class="sidebar-item">
                            <a href="<?php echo base_url('konfig_pembayaran')?>" class="sidebar-link"><i
                                    data-feather="disc" class="mdi mdi-view-quilt"></i><span class="hide-menu">
                                    Konfigurasi Pembayaran
                                </span></a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i
                            data-feather="user" class="feather-icon"></i><span class="hide-menu">Pengaturan
                            Akun</span></a>
                    <ul aria-expanded="false" class="collapse first-level">
                        <li class="sidebar-item">
                            <a href="<?php echo base_url('akun')?>" class="sidebar-link"><i data-feather="disc"
                                    class="mdi mdi-view-quit"></i><span class="hide-menu"> Akun
                                </span></a>
                        </li>
                        <li class="sidebar-item">
                            <a href="<?php echo base_url('toko')?>" class="sidebar-link"><i data-feather="disc"
                                    class="mdi mdi-view-quit"></i><span class="hide-menu"> Toko
                                </span></a>
                        </li>
                        <li class="sidebar-item">
                            <a href="<?php echo base_url('akun_bank')?>" class="sidebar-link"><i data-feather="disc"
                                    class="mdi mdi-view-quit"></i><span class="hide-menu"> Akun Bank
                                </span></a>
                        </li>
                        <li class="sidebar-item">
                            <a href="<?php echo base_url('struk')?>" class="sidebar-link"><i data-feather="disc"
                                    class="mdi mdi-view-quit"></i><span class="hide-menu">
                                    Struk </span></a>
                        </li>
                    </ul>
                </li>
                <li class="sidebar-item">
                    <a class="sidebar-link waves-effect waves-dark sidebar-link"
                        href="<?php echo base_url('auth/logout')?>" aria-expanded="false"><i data-feather="log-out"
                            class="feather-icon"></i><span class="hide-menu">Log Out</span></a>
                </li>
            </ul>
        </nav>
    </div>
</aside>